This part of the project documentation focuses on a **problem-oriented**
approach. You'll tackle common tasks that you might have.

## How to build the documentation website ?

- First you must clone the repository.
- Then you must install the documentation dependencies:
  ```sh
  poetry install --with docs
  ```
- Then you can run `mkdocs` to build and serve the website locally:
  ```sh
  poetry run mkdocs serve
  ```

## How to test the code while developing ?

- You must install the test dependencies:
  ```sh
  poetry install --with tests
  ```
- Then you can run `pytest` to run the test scripts.
  ```sh
  poetry run pytest
  ```
