This part of the project documentation focuses on a **learning-oriented**
approach. You'll learn how to get started.

## Installation

This is a standard Python package. You can install it from pypi with:

```sh
pip install kcg
```

Or you can clone this repository locally and install it from the sources.

This repository is using [poetry](https://python-poetry.org/) to build the
package, so you can run these commands in the cloned repository:

```sh
# poetry creates a new virtual environment for kcg
poetry install
# poetry lets you run kcg in the virtual env
poetry run kcg
```

## Command line

The main entry point of this tool is its command line.

Once installed, the command line `kcg` is available. Use `--help` to get
help in your terminal:

```sh
$ kcg --help
```

To generate configuration files, you need to create a “context” file containing
information about your keyboard and the location of its source files. Please
read [Configuration Files > Context](reference/configuration-files/context.md)
to get more details.

Then you can use the subcommand `generate` to generate some configuration files
for a given target. For instance, to generate keyboard files for the Linux X
system, run:

```sh
$ kcg generate /path/to/context.ini --output /output/path x
```

Do no hesitate to use the `--help` option to get more details about
subcommands.
