# Context

The “context” file is a configuration file read by KCG. It contains all the
necessary inputs to generate some keyboard configuration files.

The user must create this file, as it is not provided.

It is a file written in `INI` format (it can be named `context.ini`) and its
content is described on this page, below.

## Example

```ini
[basic_info]
name=bépo
locale=fr-FR
language=French
variant=bépo
version=1.1rc2

[files]
layout=./layout.conf
deadkeys=./deadkeys.conf
double_deadkeys=./double_deadkeys.conf
virtual_keys=./virtual_keys.conf
keys=./keys.conf
special_keys=./special_keys.conf
symbols=./symbols.conf
unicode=./UnicodeData-9.0.b6.partial.fr.txt
```

## Section `basic_info`

### Attribute `name`

### Attribute `language`

### Attribute `locale`

### Attribute `variant`

### Attribute `version`

## Section `files`

### Attribute `layout`

### Attribute `deadkeys`

### Attribute `double_deadkeys`

### Attribute `virtual_keys`

### Attribute `keys`

### Attribute `special_keys`

### Attribute `symbols`

### Attribute `unicode`

