# coding: utf-8
"""Module to parse CSV files."""

import pandas


def parse(filename):
    """Parse a CSV file.

    Returns:
        (pandas.DataFrame) The parsed CSV file as a dataframe object.
    """
    return pandas.read_csv(
        filename,
        comment="#",
        skip_blank_lines=True,
        delimiter=",",
        header=0,
    )
