# coding: utf-8
"""Generators for Linux X system."""

import pathlib

from kcg.generators.perl import run_perl_script


def generate(context: dict, output: pathlib.Path, target: str = None) -> None:
    """Generate configuration files for X system.

    Args:
        context (dict): the context as a dict (read doc/context.md).
        output (pathlib.Path): directory to write output files.
        target (str) (optional): select a single output file to generate. If not
            provided (default) all output files are generated. Can be:
                - xkb-root
                - xkb-user
                - xmodmap
                - xcompose
    """
    name = context["basic_info"]["name"]
    targets = {
        "xkb-root": ("x_xkb_root", f"{name}-system.xkb"),
        "xkb-user": ("x_xkb_user", f"{name}.xkb"),
        "xmodmap": ("x_xmodmap", f"{name}.xmodmap"),
        "xcompose": ("x_compose", "XCompose"),
    }

    for target_name, (perl_function, output_filename) in targets.items():
        if target is not None and target_name != target:
            continue
        run_perl_script(context, perl_function, output.joinpath(output_filename))

    # create a legacy file with a small substitution
    with open(output.joinpath(f"{name}.xkb"), encoding="utf-8") as fin:
        with open(
            output.joinpath(f"{name}-xorglegacy.xkb"), "w", encoding="utf-8"
        ) as fout:
            for line in fin:
                fout.write(
                    line.replace('\tinclude "pc(pc105)"', '\tinclude "pc/pc(pc105)"')
                )
