# coding: utf-8
"""Generators for Windows."""

import pathlib

from kcg.generators.perl import run_perl_script


def generate(context: dict, output: pathlib.Path, target: str = None) -> None:
    """Generate configuration files for windows.

    Args:
        context (dict): the context as a dict (read doc/context.md).
        output (pathlib.Path): directory to write output files.
        target (str) (optional): select a single output file to generate. If not
            provided (default) all output files are generated. Can be:
                - azerty
                - bépo
                - qwertz
    """
    name = context["basic_info"]["name"]
    targets = {
        "azerty": [
            ("win_msklc_azerty", f"{name}-azerty-kbd.klc", "utf-16le"),
            ("win_msklc_azerty", f"{name}-azerty.klc", "utf-16"),
        ],
        "bépo": [
            ("win_msklc_bepo", f"{name}-kbd.klc", "utf-16le"),
            ("win_msklc_bepo", f"{name}.klc", "utf-16"),
        ],
        "qwertz": [
            ("win_msklc_qwertz", f"{name}-qwertz-kbd.klc", "utf-16le"),
            ("win_msklc_qwertz", f"{name}-qwertz.klc", "utf-16"),
        ],
    }

    for target_name, subtargets in targets.items():
        if target is not None and target_name != target:
            continue
        for perl_function, output_filename, encoding in subtargets:
            run_perl_script(
                context, perl_function, output.joinpath(output_filename), encoding
            )
